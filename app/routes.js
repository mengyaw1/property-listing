import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SiteContainer from "layouts/Site-Container";

require("scss/style.scss");

class Routes extends React.Component {
  render() {
    const site_container = (props) => { return <SiteContainer {...props} />; };

    return (
      <Router>
        <Route render={site_container} />
      </Router>
    );
  }
}

export default Routes;
