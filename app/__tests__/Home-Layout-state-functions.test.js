import { add_property, remove_property } from 'layouts/Home-Layout-state-functions'
import property_data from 'redux_and_actions/mock.json'

// Follow the user flow diagram in specification file
test('add_property adds a property from results list to saved properties list', () => {
  // Add property with id of 1 from results list to saved properties list
  const property_to_add = property_data.results[0]
  const start_state = {
    property_id_saved: [],
    property_saved: [property_data.saved[0]]
  }
  const end_state = add_property(start_state, property_to_add)

  expect(end_state.property_id_saved).toEqual(["1"])
  expect(end_state.property_saved).toEqual([property_data.saved[0], property_to_add])
});

test('remove_property removes a property from saved properties list', () => {
  // Remove saved property with id of 4 from saved properties list
  const start_state = {
    property_id_saved: ["1"],
    property_saved: [property_data.saved[0], property_data.results[0]]
  }
  const end_state = remove_property(start_state, property_data.saved[0])

  expect(end_state.property_id_saved).toEqual(["1"])
  expect(end_state.property_saved).toEqual([property_data.results[0]])
});

test('remove_property removes a property from saved properties list', () => {
  // Remove saved property with id of 1 from saved properties list
  const start_state = {
    property_id_saved: ["1"],
    property_saved: [property_data.results[0]]
  }
  const end_state = remove_property(start_state, property_data.results[0])

  expect(end_state.property_id_saved).toEqual([])
  expect(end_state.property_saved).toEqual([])
});
