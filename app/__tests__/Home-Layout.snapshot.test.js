import React from 'react'
import renderer from 'react-test-renderer'

import { HomeLayout } from 'layouts/Home-Layout'
import property_data from 'redux_and_actions/mock.json'

// Test HomeLayout without Redux store
test('HomeLayout renders correctly', () => {
  const get_property_listing = jest.fn();
  const layout = renderer.create(
    <HomeLayout
      property_listing={property_data}
      get_property_listing={get_property_listing}
    />
  )
  let tree = layout.toJSON()
  expect(tree).toMatchSnapshot()
})
