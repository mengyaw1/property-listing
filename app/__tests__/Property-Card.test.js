import React from 'react'
import { mount } from 'enzyme'

import PropertyCard from 'components/Property-Card'
import property_data from 'redux_and_actions/mock.json'

test('PropertyCard in results list calls handle_click when button is clicked if property not saved', () => {
  // Expect Add Property button to be rendered and handle_click function to be called when button is clicked
  const property = property_data.results[0];
  const handle_click = jest.fn();
  const property_card = mount(
    <PropertyCard
      property={property}
      action='add'
      handle_click={handle_click}
    />
  );

  const button = property_card.find('.btn.add-btn');
  button.simulate('click');
  expect(handle_click).toHaveBeenCalled();
});

test('PropertyCard in results list do not call handle_click when button is clicked if property already saved', () => {
  // Expect Saved button to be rendered and handle_click function not to be called when button is clicked since its already saved
  const property = property_data.results[0];
  const handle_click = jest.fn();
  const property_card = mount(
    <PropertyCard
      property={property}
      action={null}
      handle_click={handle_click}
    />
  );

  const button = property_card.find('.btn.saved-btn');
  button.simulate('click');
  expect(handle_click).not.toHaveBeenCalled();
});


test('PropertyCard in saved list calls handle_click when button is clicked', () => {
  // Expect Remove Property button to be rendered and handle_click function to be called when button is clicked
  const property = property_data.saved[0];
  const handle_click = jest.fn();
  const property_card = mount(
    <PropertyCard
      property={property}
      action="remove"
      handle_click={handle_click}
    />
  );

  const button = property_card.find('.btn.remove-btn');
  button.simulate('click');
  expect(handle_click).toHaveBeenCalled();
});
