import React from 'react'
import renderer from 'react-test-renderer'

import PropertyCard from 'components/Property-Card'
import property_data from 'redux_and_actions/mock.json'

// Test PropertyCartd component with different action type
test('PropertyCard component renders correctly', () => {
  const property = property_data.results[0];
  const component = renderer.create(
    <PropertyCard
      property={property}
      action='add'
    />
  )
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('PropertyCard component renders correctly', () => {
  const property = property_data.results[0]
  const component = renderer.create(
    <PropertyCard
      property={property}
      action='remove'
    />
  )
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
});

test('PropertyCard component renders correctly', () => {
  const property = property_data.results[0]
  const component = renderer.create(
    <PropertyCard
      property={property}
      action={null}
    />
  )
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
});
