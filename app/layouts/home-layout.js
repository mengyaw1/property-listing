import React from 'react';
import { connect } from 'react-redux'

import { get_property_listing } from 'redux_and_actions/actions'

import Loader from 'components/Loader'
import PropertyCard from 'components/Property-Card'
import { add_property, remove_property } from 'layouts/Home-Layout-state-functions'

export class HomeLayout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      property_id_saved: [], // Store the id of properties that are added to saved properties list from results list
      property_saved: [] // Store property objects that are saved in saved properties list
    }
  }

  componentWillMount() {
    this.props.get_property_listing();
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.property_listing != nextProps.property_listing && nextProps.property_listing.success) {
      this.setState({
        property_saved: nextProps.property_listing.data.saved
      })
    }
  }

  add_property = (property) => {
    this.setState(add_property(this.state, property));
  }

  remove_property = (property, index) => {
    this.setState(remove_property(this.state, property, index));
  }

  render() {
    if(this.props.property_listing == null) { // Render loader when waiting for responce
      return <div className="home-layout"><div className="loading"><Loader /><div>One moment please ...</div></div></div>
    }
    else if(!this.props.property_listing.success) { // Render error message when data fetching went wrong
      return <div className="home-layout"><div className="error-message">{ this.props.property_listing.message }</div></div>
    }
    return ( // Render content when data is fetched successfully
      <div className="home-layout">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-md-6">
              <div className="property-results">
                <div className="results-title">Results</div>
                <div className="results-list">
                  {
                    this.props.property_listing.data.results.length > 0 ?
                    this.props.property_listing.data.results.map((property, index) =>
                      <PropertyCard
                        property={property}
                        action={this.state.property_id_saved.indexOf(property.id) < 0 ? 'add' : null} // Property can not be added again if its already in saved properties list
                        handle_click={()=>this.add_property(property, index)}
                        key={index}
                      />
                    ) :
                    'Sorry, no available property results.'
                  }
                </div>
              </div>
            </div>
            <div className="col-xs-12 col-md-6">
              <div className="property-saved">
                <div className="saved-title">Saved Properties</div>
                <div className="saved-list">
                  {
                    this.state.property_saved.length > 0 ?
                    this.state.property_saved.map((property, index) =>
                      <PropertyCard
                        property={property}
                        action='remove'
                        handle_click={()=>this.remove_property(property, index)}
                        key={index}
                      />
                    ) :
                    'You have no saved properties.'
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    property_listing: state.home.property_listing
  }
}

function mapDispatchToProps (dispatch) {
  return {
    get_property_listing: (data) => dispatch(get_property_listing(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeLayout)
