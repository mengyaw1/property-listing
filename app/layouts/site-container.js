import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'

import HomeLayout from 'layouts/Home-Layout'

// Site container is designed for sharing components across the whole website, like site header, site footer, etc,.
class SiteContainer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="site-container">
        <div className="site-body">
          <Switch>
            <Route exact path="/" render={(props) => <HomeLayout {...props} />} />
            <Redirect to="/" />
          </Switch>
        </div>
      </div>
    )
  }
}

export default SiteContainer;
