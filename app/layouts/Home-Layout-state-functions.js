import update from 'immutability-helper'

export function add_property(state, property) {
  let newState = update(state, {property_id_saved: {$push: [property.id]}, property_saved: {$push: [property]}});
  return newState
}

export function remove_property(state, property, index) {
  let removed_property_id_index = state.property_id_saved.indexOf(property.id);
  let newState = update(state, {property_saved: {$splice: [[ index, 1 ]]}});
  if(removed_property_id_index > -1) {
    newState = update(newState, {property_id_saved: {$splice: [[removed_property_id_index, 1]]}})
  }
  return newState;
}
