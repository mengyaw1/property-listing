// import axios from "axios"
import property_data from 'redux_and_actions/mock.json'

export function get_property_listing(data) {
  // Return our mock data instead of making API request
  return { success: true, data: property_data, message: 'Property listing retrieved successfully.' }
}
