import { combineReducers } from "redux"
import { GET_PROPERTY_LISTING } from "./actions"

const init_property_listing = () =>
  ({
    property_listing: null
  });

function property_listing_reducer(state = init_property_listing(), action) {
  switch (action.type) {
    case GET_PROPERTY_LISTING:
      return {
        ...state,
        property_listing: action.payload
      };

    default:
      return state
  }
}

export const rootReducer = combineReducers({
  home: property_listing_reducer
});
