import React from 'react'

class PropertyCard extends React.Component {
  constructor(props) {
    super(props)
  }

  render_property_button = () => {
    if(this.props.action) {
      return <button className={`btn ${this.props.action}-btn`} onClick={this.props.handle_click}>{ this.props.action } Property</button>
    }
    else {
      return <button className="btn saved-btn" onClick={event=>event.preventDefault()}>Saved</button>
    }
  }

  render() {
    return (
      <div className="property-card">
        <div className="property-logo" style={{ backgroundColor: this.props.property.agency.brandingColors.primary }}>
          <img src={this.props.property.agency.logo} alt="Agency Logo"/>
        </div>
        <div className="property-image">
          <img src={this.props.property.mainImage} alt="Property Image" />
        </div>
        <div className="property-price">{ this.props.property.price }</div>
        { this.render_property_button() }
      </div>
    )
  }
}

export default PropertyCard;
