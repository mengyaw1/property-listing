# property-listing

## Environment requirements:

- [Node.js](https://nodejs.org/en/)
- [npm](https://www.npmjs.com)
- [Webpack](https://webpack.github.io/)

## Installation:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install node openssl
npm install -g webpack
git clone https://mengyaw1@bitbucket.org/mengyaw1/property-listing.git ~/folder/
cd ~/folder/property-listing
npm install
npm start
```

## Run locally as development, with HMR enabled:
`npm start`

## Run as production:
`npm run start:prod`

Note that this includes building, which takes a while

## Build for production:
`npm run build`

## Run test:
`npm run test`

## Responsive breakpoints:
```
{
	xs: '0',
	sm: '576px',
	md: '768px',
	lg: '992px',
	xl: '1200px'
}
```

### Examples
```
@include media-breakpoint-up(sm)           { } // Small and up
@include media-breakpoint-down(md)         { } // Medium and down
@include media-breakpoint-between(md, lg)  { } // Between medium and large
```